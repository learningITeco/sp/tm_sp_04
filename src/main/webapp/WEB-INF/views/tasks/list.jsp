<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<body>

	<div class="container">

		<c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>

		<h1>All Tasks</h1>

        <spring:url value="/tasks/add" var="addUrl" />
        <button class="btn btn-primary" onclick="location.href='${addUrl}'">Add</button>

		<table class="table table-striped">
			<thead>
				<tr>
				    <th>No.</th>
					<th>#ID</th>
					<th>NAME</th>
					<th>DESCRIPTION</th>
					<th>PROJECT</th>
					<th>USER</th>
					<th>DATE START</th>
					<th>DATE FINISH</th>
					<th>STATUS</th>
				</tr>
			</thead>

			<c:forEach var="task" items="${taskList}" varStatus="status">
			    <tr>
			        <td align="center">${status.count}</td>
                    <td><input name="task[${status.index}].id" value="${task.id}"/></td>
                    <td><input name="task[${status.index}].name" value="${task.name}"/></td>
                    <td><input name="task[${status.index}].description" value="${task.description}"/></td>
                    <td><input name="task[${status.index}].project" value="${task.project}"/></td>
                    <td><input name="task[${status.index}].user.getLogin()" value="${task.user.getLogin()}"/></td>
                    <td><input name="task[${status.index}].dateStart" value="${task.dateStart}"/></td>
                    <td><input name="task[${status.index}].dateFinish" value="${task.dateFinish}"/></td>
                    <td><input name="task[${status.index}].status" value="${task.status}"/></td>

                    <td>
                      <spring:url value="/tasks/${task.id}/delete" var="deleteUrl" />
                      <spring:url value="/tasks/${task.id}/update" var="updateUrl" />

                      <button class="btn btn-primary"
                                              onclick="location.href='${updateUrl}'">Update</button>
                      <button class="btn btn-danger"
                                              onclick="location.href='${deleteUrl}'">Delete</button>
                    </td>
			    </tr>
			</c:forEach>
		</table>

	</div>
</body>
</html>