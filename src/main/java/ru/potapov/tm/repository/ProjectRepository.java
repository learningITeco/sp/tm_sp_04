package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Project;

import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@Repository
@ManagedBean(name = "projectRepository", eager = true)
public class ProjectRepository extends AbstractRepository<Project>{
    @Nullable
    @Override
    public Project findOne(@NotNull String id) {
        @Nullable Project project = null;
        @Nullable final String query = "from Project where id =:id";
        @NotNull TypedQuery<Project> typedQuery = entityManager.createQuery(query, Project.class).setParameter("id", id);
        project = typedQuery.getSingleResult();

        return project;
    }

    @Override
    public @NotNull Collection<Project> findAll() {
        @NotNull  TypedQuery<Project> typedQuery = entityManager.createQuery("From Project", Project.class);
        @NotNull final Collection<Project> collection = typedQuery.getResultList();
        return collection;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Project ");
    }
}
