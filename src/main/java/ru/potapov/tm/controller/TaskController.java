package ru.potapov.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@Controller
//@RequestMapping(value = "/task")
@ComponentScan(basePackages = "ru.potapov.tm")
public class TaskController {

    @NotNull private final Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    @NotNull ServiceLocator serviceLocator;

    public TaskController() {}

    // list page
    @RequestMapping(value = "/task", method = RequestMethod.POST)
    public String showAllTasks(Model model) {
        logger.debug("showAllTasks()");
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        return "tasks/list";
    }

    //tasks
    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    public String saveOrUpdateTask(@ModelAttribute("taskForm") @Validated @NotNull Task task,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {

        logger.debug("saveOrUpdateTask() : {}", task);
        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "tasks/taskform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(task.isNew()){
                redirectAttributes.addFlashAttribute("msg", "TaskDto added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "TaskDto updated successfully!");
            }
            serviceLocator.getTaskService().merge(task);

            populateDefaultModel(model);

            return "tasks/list";
        }
    }

    // show add task form
    @RequestMapping(value = "/tasks/add", method = RequestMethod.GET)
    public String showAddTaskForm(Model model) {

        logger.debug("showAddTaskForm()");

        @NotNull Task task = new Task();
        // set default value
        task.setName("");
        task.setDescription("D22");
        task.setProject(null);
        task.setStatus(Status.Planned);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        model.addAttribute("taskForm", task);
        populateDefaultModel(model);

        populateDefaultModel(model);

        return "tasks/taskform";
    }

    // show update form
    @RequestMapping(value = "/tasks/{id}/update", method = RequestMethod.GET)
    public String showUpdateTaskForm(@PathVariable("id") String id, Model model) {

        logger.debug("showUpdateTaskForm() : {}", id);

        @NotNull final Task task = serviceLocator.getTaskService().findOne(id);
        model.addAttribute("taskForm", task);

        populateDefaultModel(model);

        return "tasks/taskform";
    }

    // delete task
    @RequestMapping(value = "/tasks/{id}/delete", method = RequestMethod.GET)
    public String deleteTask(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("deleteTask() : {}", id);

        @NotNull final Task task = serviceLocator.getTaskService().findOne(id);
        if (task != null)
            serviceLocator.getTaskService().remove(task);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "TaskDto is deleted!");

        populateDefaultModel(model);

        return "tasks/list";        
    }


    private void populateDefaultModel(Model model) {
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        model.addAttribute("sessionList", serviceLocator.getSessionService().findAll());

        model.addAttribute("statuses", Status.values());
        model.addAttribute("roles", RoleType.values());
    }
}
