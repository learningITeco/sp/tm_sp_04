package ru.potapov.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.UserDto;
import ru.potapov.tm.entity.User;

import java.util.Collection;

@RestController
@RequestMapping("rest/user")
public class UserRestController {
    @Autowired
    @NotNull ServiceLocator serviceLocator;

    @GetMapping(produces = "application/json")
    public ResponseEntity<Collection<UserDto>> getUserList(){
        Collection<User>     userList = serviceLocator.getUserService().findAll();
        Collection<UserDto>  userDtoList = serviceLocator.getUserService().collectionEntityToDto(userList);
        return ResponseEntity.ok(userDtoList);
    }

    @GetMapping(path = "{userId}", produces = "application/json")
    public ResponseEntity<UserDto> getUser(@PathVariable String userId){
        try {
            User user         = serviceLocator.getUserService().findOne(userId);
            UserDto userDto   = serviceLocator.getUserService().entityToDto(user);
            return ResponseEntity.ok(userDto);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto){
        User user = serviceLocator.getUserService().dtoToEntity(userDto);
        userDto.setId(serviceLocator.getUserService().merge(user));
        return ResponseEntity.ok(userDto);
    }

    @PutMapping(
            path = "{userId}",
            consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<Void> modifyUser(@PathVariable String userId, @RequestBody UserDto userDto){
        User user = serviceLocator.getUserService().dtoToEntity(userDto);
        if (user == null)
            return ResponseEntity.notFound().build();

        serviceLocator.getUserService().merge(user);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable String userId){
        User user = serviceLocator.getUserService().findOne(userId);
        if (user == null)
            return ResponseEntity.notFound().build();

        serviceLocator.getUserService().remove(user);
        return ResponseEntity.noContent().build();
    }
}
