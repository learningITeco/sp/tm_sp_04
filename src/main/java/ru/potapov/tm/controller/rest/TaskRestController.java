package ru.potapov.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.TaskDto;
import ru.potapov.tm.entity.Task;

import java.util.Collection;

@RestController
@RequestMapping("rest/task")
public class TaskRestController {
    @Autowired
    @NotNull ServiceLocator serviceLocator;

    @GetMapping(produces = "application/json")
    public ResponseEntity<Collection<TaskDto>> getTaskList(){
        Collection<Task>     taskList = serviceLocator.getTaskService().findAll();
        Collection<TaskDto>  taskDtoList = serviceLocator.getTaskService().collectionEntityToDto(taskList);
        return ResponseEntity.ok(taskDtoList);
    }

    @GetMapping(path = "{taskId}", produces = "application/json")
    public ResponseEntity<TaskDto> getTask(@PathVariable String taskId){
        try {
            Task task         = serviceLocator.getTaskService().findOne(taskId);
            TaskDto taskDto   = serviceLocator.getTaskService().entityToDto(task);
            return ResponseEntity.ok(taskDto);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<TaskDto> createTask(@RequestBody TaskDto taskDto){
        Task task = serviceLocator.getTaskService().dtoToEntity(taskDto);
        taskDto.setId(serviceLocator.getTaskService().merge(task));
        return ResponseEntity.ok(taskDto);
    }

    @PutMapping(
            path = "{taskId}",
            consumes = {MediaType.APPLICATION_JSON_VALUE, "application/json"})
    public ResponseEntity<Void> modifyTask(@PathVariable String taskId, @RequestBody TaskDto taskDto){
        Task task = serviceLocator.getTaskService().dtoToEntity(taskDto);
        if (task == null)
            return ResponseEntity.notFound().build();

        serviceLocator.getTaskService().merge(task);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{taskId}")
    public ResponseEntity<Void> deleteTask(@PathVariable String taskId){
        Task task = serviceLocator.getTaskService().findOne(taskId);
        if (task == null)
            return ResponseEntity.notFound().build();

        serviceLocator.getTaskService().remove(task);
        return ResponseEntity.noContent().build();
    }
}
