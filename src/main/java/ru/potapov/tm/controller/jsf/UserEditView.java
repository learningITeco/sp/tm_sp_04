package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtUserEditView", eager = true)
@ViewScoped
public class UserEditView extends SpringBeanAutowiringSupport implements Serializable {
    private List<User> users;
    private List<User> users2;
    private List<RoleType> roleTypes;

    @Autowired
    @NotNull
    IUserService userService;

    @PostConstruct
    public void init() {
        users = (List<User>) userService.findAll();
        users2 = (List<User>) userService.findAll();
        users = (List<User>) userService.findAll();
        roleTypes = getRoles();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<User> getUsers2() {
        return users2;
    }

    public List<RoleType> getRoles(){
        return Arrays.asList(RoleType.values());
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        User user = (User)event.getObject();
        FacesMessage msg = new FacesMessage("User Edited", user.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        User user = (User)event.getObject();
        FacesMessage msg = new FacesMessage("Edit Cancelled", user.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
