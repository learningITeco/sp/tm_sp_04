package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtUserAddRowView", eager = true)
@RequestScoped
public class UserAddRowView extends SpringBeanAutowiringSupport implements Serializable {
    private List<User> users;

    @Autowired
    @NotNull
    IUserService userService;

    @PostConstruct
    public void init() {
        users = (List<User>) userService.findAll();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onRowEdit(RowEditEvent event) {
        User user = (User)event.getObject();
        userService.merge(user);
        FacesMessage msg = new FacesMessage("Car Edited", user.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        User user = (User)event.getObject();
        FacesMessage msg = new FacesMessage("Edit Cancelled", user.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onAddNew() {
        // Add one new car to the table:
        User user2Add = new User();
        user2Add.setLogin("New user");
        user2Add.setHashPass("asdasdasdasdsdasdasdasd");
        user2Add.setRoleType(RoleType.User);

        userService.merge(user2Add);

        FacesMessage msg = new FacesMessage("New Car added", user2Add.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
