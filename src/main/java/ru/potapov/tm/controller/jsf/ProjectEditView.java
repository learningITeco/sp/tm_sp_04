package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.Status;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
//@Named("dtEditView")
@ManagedBean(name = "dtEditView", eager = true)
@ViewScoped
public class ProjectEditView  extends SpringBeanAutowiringSupport implements Serializable {
    private List<Project> projects;
    private List<Project> projects2;
    private List<Status> statuses;
    private List<User> users;

    @Autowired
    @NotNull
    IProjectService projectService;

    @Autowired
    @NotNull
    IUserService userService;

    @PostConstruct
    public void init() {
        projects = (List<Project>) projectService.findAll();
        projects2 = (List<Project>) projectService.findAll();
        users = (List<User>) userService.findAll();
        statuses = getStatuses();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Project> getProjects2() {
        return projects2;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        Project project = (Project)event.getObject();
        FacesMessage msg = new FacesMessage("Car Edited", project.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        Project project = (Project)event.getObject();
        FacesMessage msg = new FacesMessage("Edit Cancelled", project.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
