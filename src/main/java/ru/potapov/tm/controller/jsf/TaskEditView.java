package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.Status;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtTaskEditView", eager = true)
@ViewScoped
public class TaskEditView extends SpringBeanAutowiringSupport implements Serializable {
    private List<Task> tasks;
    private List<Task> tasks2;
    private List<Status> statuses;
    private List<User> users;
    private List<Project> projects;

    @Autowired
    @NotNull
    ITaskService taskService;

    @Autowired
    @NotNull
    IProjectService projectService;

    @Autowired
    @NotNull
    IUserService userService;

    @PostConstruct
    public void init() {
        tasks = (List<Task>) taskService.findAll();
        tasks2 = (List<Task>) taskService.findAll();
        users = (List<User>) userService.findAll();
        projects = (List<Project>) projectService.findAll();
        statuses = getStatuses();
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public List<Task> getTasks2() {
        return tasks2;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        Task task = (Task)event.getObject();
        FacesMessage msg = new FacesMessage("Task Edited", task.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        Task task = (Task)event.getObject();
        FacesMessage msg = new FacesMessage("Edit Cancelled", task.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
