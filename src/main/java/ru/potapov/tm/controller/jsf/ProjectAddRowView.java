package ru.potapov.tm.controller.jsf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.enumeration.Status;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@ManagedBean(name = "dtAddRowView", eager = true)
@RequestScoped
public class ProjectAddRowView extends SpringBeanAutowiringSupport implements Serializable {
    private List<Project> projects;

    @Autowired
    @NotNull
    IProjectService projectService;

    @PostConstruct
    public void init() {
        projects = (List<Project>) projectService.findAll();
    }

    public List<Project> getProjects() {
        return projects;
    }

    public List<Status> getStatuses(){
        return Arrays.asList(Status.values());
    }

    public void onRowEdit(RowEditEvent event) {
        Project project = (Project)event.getObject();
        projectService.merge(project);
        FacesMessage msg = new FacesMessage("Car Edited", project.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        Project project = (Project)event.getObject();
        FacesMessage msg = new FacesMessage("Edit Cancelled", project.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onAddNew() {
        // Add one new project to the table:
        Project project2Add = new Project();
        project2Add.setName("New project");
        project2Add.setDescription("Set description there");
        project2Add.setStatus(Status.Planned);
        project2Add.setDateStart(new Date());
        project2Add.setDateFinish(new Date());

        projectService.merge(project2Add);

        FacesMessage msg = new FacesMessage("New Project added", project2Add.getId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
