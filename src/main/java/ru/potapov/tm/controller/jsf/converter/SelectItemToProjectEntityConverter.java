package ru.potapov.tm.controller.jsf.converter;

import ru.potapov.tm.entity.Project;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.List;
import java.util.function.Predicate;

//@FacesConverter(value = "SelectItemToProjectEntityConverter")
@FacesConverter(forClass = Project.class)
public class SelectItemToProjectEntityConverter implements Converter {


    @Override
    public Object getAsObject(FacesContext ctx, UIComponent comp, String value) {
        Object o = null;
        if (!(value == null || value.isEmpty())) {
            o = this.getSelectedItemAsEntity(comp, value);
        }
        return o;
    }

    @Override
    public String getAsString(FacesContext ctx, UIComponent comp, Object value) {
        String s = "";
        if (value != null) {
            s = ((Project) value).getId().toString();
        }
        return s;
    }

    private Project getSelectedItemAsEntity(UIComponent comp, String value) {
        Project item = null;

        List<Project> selectItems = null;
        for (UIComponent uic : comp.getChildren()) {
            if (uic instanceof UISelectItems) {
                String itemId = value;
                selectItems = (List<Project>) ((UISelectItems) uic).getValue();

                if (itemId != null && selectItems != null && !selectItems.isEmpty()) {
                    Predicate<Project> predicate = i -> i.getId().equals(itemId);
                    item = selectItems.stream().filter(predicate).findFirst().orElse(null);
                }
            }
        }

        return item;
    }
}
