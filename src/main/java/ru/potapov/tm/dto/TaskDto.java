package ru.potapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.AbstractEntity;
import ru.potapov.tm.enumeration.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TaskDto implements Cloneable, Serializable {
    @Nullable private String        id;
    @Nullable private String        name;
    @Nullable private String        description;
    @Nullable private String        userId;
    @Nullable private Date          dateStart;
    @Nullable private Date          dateFinish;
    @Nullable private Status        status;
    @Nullable private String        projectId;
}
