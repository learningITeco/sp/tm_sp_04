package ru.potapov.tm.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.AbstractEntity;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.Status;

import java.util.Date;

@Data
@Getter
@Setter
@NoArgsConstructor
public final class ProjectDto {
    @Nullable private String    id;
    @Nullable private String    name;
    @Nullable private String    description;
    @Nullable private String    userId;
    @Nullable private Status    status;
    @Nullable private Date      dateStart;
    @Nullable private Date      dateFinish;
}
